# How to handle data in your repository

Version controlling data in your repo is not pretty because you heavily increase its size.

Tools like Git-LFS try to solve this problem, but it's still premature.

A good (working) solution is to use submodules, which basically means, a repo inside a repo.

## Step 1. Create a data repo [here](https://gitlab.kwant-project.org/projects/new)
Call it something like: `INSERT_ORIGINAL_NAME_OF_REPO_HERE_data`. 

Go to its settings -> Members -> Set a group to share -> Quantum Tinkerer, with Developer access.

## Step 2. Put a file in your new repo.
To initialize a submodule there needs to be something in there. Just add a `README.md` in the web interface or something.

## Step 3. Add data repo as submodule
* `cd` into your module
* copy the `git` url of your data repo and run `git submodule add url_here data`, for example: `git submodule add ssh://git@gitlab.kwant-project.org:443/basnijholt/proximity_disorder_data.git data` 
