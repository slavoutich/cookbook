import warnings
import numpy as np


def organize_matrix_1d(sys, matrix):
    """Organize matrix according to sys sites order."""
    num_nodes = sys.graph.num_nodes
    num_bands = int(matrix.shape[0]/num_nodes)
    x = [sys.sites[i].pos[0] for i in range(num_nodes)]

    indx = np.argsort(x)
    indx = [num_bands*indx[i]+j for i in range(len(indx)) for j in range(num_bands)]
    return matrix[:, indx][indx,:]


def organize_matrix_2d(sys, matrix):
    """Organize matrix according to sys sites order."""
    num_nodes = sys.graph.num_nodes
    num_bands = int(matrix.shape[0]/num_nodes)
    x = np.array([sys.sites[i].pos for i in range(num_nodes)])

    indx = np.lexsort([x[:,0], x[:,1]])
    indx = [num_bands*indx[i]+j for i in range(len(indx)) for j in range(num_bands)]

    return matrix[:, indx][indx,:]


def organize_matrix(sys, matrix):
    """Organize matrix according to sys sites order."""
    warnings.warn('This function is not tested. Please leave me some feedback!'+
                  'If you want something that works for sure use either'+
                  '`organize_matrix_2d` or `organize_matrix_1d`')


    num_nodes = sys.graph.num_nodes
    num_bands = int(matrix.shape[0]/num_nodes)
    x = np.array([sys.sites[i].pos for i in range(num_nodes)])

    indx = np.lexsort([x[:, j] for j in range(x.shape[1])])
    indx = [num_bands*indx[i]+j for i in range(len(indx)) for j in range(num_bands)]

    return matrix[:, indx][indx,:]
