# Set-up your connection to hpc05 and explain usage

# Requirements
* Follow **all** instructions from the [ssh-setup tutorial](https://gitlab.kwant-project.org/qt/cookbook/blob/master/ssh-setup.md)
* Test if you can `ssh` **password less** `your_computer -> io -> hpc05` with `ssh io` and then `ssh hpc05`.

To provide the `ssh` key password only once on login in to the machine do
```
echo 'eval `keychain --eval id_rsa`' >> ~/.bash_profile; source ~/.bash_profile
```
# Preparations
## Step 1. Set-up the `.bash_profile` and install ``python`` environment on the `hpc05`
Follow the steps in [this repo: init_hpc05](https://gitlab.kwant-project.org/basnijholt/init_hpc05)

## Step 2. Set-up a ipyparallel profile on `hpc05` and make a connection.
*If not on `io`, install `hpc05` with:*
`conda install -c conda-forge hpc05`

Open a notebook or terminal on `io` and run
```
import hpc05
hpc05.create_remote_pbs_profile()
```
If you get an error message read it. Probaly it will tell you to run this command on `io`:
```
rm -f ~/ssh-agent.socket; ssh-agent -a ~/ssh-agent.socket;export SSH_AUTH_SOCK=~/ssh-agent.socket; ssh-add
```
Then try `hpc05.create_remote_pbs_profile()` again.

# Usage

## Starting engines on hpc05 via `io`

```python
import hpc05
client, dview, lview = hpc05.start_remote_and_connect(100, folder='~/Work/your_folder_with_modules_on_hpc05/')
```

## See an example: [`simple_example.md`](https://gitlab.kwant-project.org/qt/cookbook/blob/master/hpc05/simple_example.md)

## Kill your engines, three options:
* From your notebook run `hpc05.kill_remote_ipcluster()`
* From your notebook run `client.shutdown(hub=True)`
* Open a terminal at `hpc05` and simply type `del` to kill all of your engines (and ipcontroller).

## Keeping the `io` and `hpc05` environments the same
* on `hpc05` to get the latest `python3` and `dev` environments ([use files from *Preparations Step 1.*](https://gitlab.kwant-project.org/basnijholt/init_hpc05)): `bash ~/init_hpc05/install_conda.sh`
* on `io` to get the latest `python3` environment: restart your server by clicking on the [Stop My Server](https://io.quantumtinkerer.tudelft.nl/hub/home) button
* on `io` to get the latest `dev` environment, `bash /environments/install_dev.sh`
