# Parallel computing with IPython cookbook

[ipyparallel documentation](https://ipyparallel.readthedocs.org/en/latest/) - just in case

## Configuring your engines

To use some cool features like:
* ``custom virtualenv`` 
* ``startup scripts`` 

you must create custom profile. To do this follow [this instruction](https://gitlab.com/meso/admin-notes/blob/master/howtos/kernels_engines.md) in ``meso-admin/howtos``.

In case previous link doesn't work: [permalink](https://gitlab.com/meso/admin-notes/blob/3decbd26dd8c649d02664e0c8ae35874dc663d45/howtos/kernels_engines.md) (may be outdated!!!).

# Two scheme of using ipyparallel

## Development scheme

Using mostly interactive definitions with ``%%px`` magics
* making lots of small changes in the code
* applying new code without restarting kernels
* almost straightforward to use
* not flexible for later arriving engines (hpc05 reality)

Examples:
* [Example 1](content/develop1.ipynb) - inside notebook definitions
* [Example 2](content/develop2.ipynb) - external files definitions



## Stable scheme

Main changes:
* Flexible to newly arriving engines
* Version controlling code
* Change in code requires restarting ipcluster!

Recipe:
* Physically move your code to cluster (git clone it there)
* Set path to your code and import it in startup script: [example from my project](content/startup.py)

[Here](content/stable1.ipynb) is example how I do it.